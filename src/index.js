import {header} from '.modules/header.js';
import {footer} from '.modules/footer.js';
import {LunchGenerator} from '.modules/lunchgenerator.js';
 
 
class App{
  constructor(){
    this.renderTemplate();
    LunchGenerator.randomizedLunches();
   } 
  
  renderTemplate(){
   
    const template = 
      <h1>${header.pasta }</h1>;
      <h2>${header.subtitle}</h2>;
      <p>${header.para}</p>;


      <footer>${footer.final}</footer>;
      <h2>${footer.contact}</h2>;
      <p>${footer.thanks}</p>;

      <p>${LunchGenerator.options}</p>
      
    document.body.innerHTML = template;

  }
}

let App= new App();




