class footer{
    constructor(end, contact, thanks){
    this.end=end;
    this.contact=contact;
    this.thanks=thanks;
    }
}

export const footer = new footer("Sydney Allen Project", "sallen97@ufl.edu", "Thank you for a great semester");

